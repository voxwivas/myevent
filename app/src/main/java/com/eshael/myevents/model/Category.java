package com.eshael.myevents.model;

/**
 * Created by Raphael on 6/11/2016.
 */
public class Category {

    private String event_category;

    public Category() {
    }

    public String getEvent_category() {
        return event_category;
    }

    public void setEvent_category(String event_category) {
        this.event_category = event_category;
    }
}
