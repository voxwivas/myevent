package com.eshael.myevents.model;

/**
 * Created by Raphael on 6/4/2016.
 */
public class Price {

    private Double single;
    private Double couple;
    private Double gold;
    private Double corporate;
    private Double silver;
    private String id;

    public Price() {
    }

    public Double getSingle() {
        return single;
    }

    public void setSingle(Double single) {
        this.single = single;
    }

    public Double getCouple() {
        return couple;
    }

    public void setCouple(Double couple) {
        this.couple = couple;
    }

    public Double getGold() {
        return gold;
    }

    public void setGold(Double gold) {
        this.gold = gold;
    }

    public Double getCorporate() {
        return corporate;
    }

    public void setCorporate(Double corporate) {
        this.corporate = corporate;
    }

    public Double getSilver() {
        return silver;
    }

    public void setSilver(Double silver) {
        this.silver = silver;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
