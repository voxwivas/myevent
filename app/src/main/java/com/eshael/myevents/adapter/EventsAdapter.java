package com.eshael.myevents.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshael.myevents.EventDetail;
import com.eshael.myevents.R;
import com.eshael.myevents.model.Event;
import com.eshael.myevents.util.Network;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 6/4/2016.
 */
public class EventsAdapter extends BaseAdapter implements Network {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Event> eventList = new ArrayList<>();

    public EventsAdapter(Context context, List<Event> eventList) {
        this.context = context;
        this.eventList = eventList;
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(layoutInflater == null)
            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = layoutInflater.inflate(R.layout.event_row,null);

        Event event = eventList.get(position);

        TextView tittle = (TextView) convertView.findViewById(R.id.ptitle);
        TextView promoter = (TextView) convertView.findViewById(R.id.promoter);
        TextView venue = (TextView) convertView.findViewById(R.id.venue);
        TextView start = (TextView) convertView.findViewById(R.id.start);
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.pthumbnail);

        tittle.setText(event.getName());
        promoter.setText("Promoter: "+event.getPromoter());
        start.setText("Start time : " + event.getStart_time());
        venue.setText("Venue : " + event.getVenue_name());
        Picasso.with(context).load(BASE_THUMB+event.getThumbnail()).into(thumbnail);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EventDetail.class);
                intent.putExtra("thumb",eventList.get(position).getThumbnail());
                intent.putExtra("id",eventList.get(position).getId());
                intent.putExtra("desc",eventList.get(position).getDescription());
                intent.putExtra("title",eventList.get(position).getName());
                intent.putExtra("prom",eventList.get(position).getUser_promoter());
                intent.putExtra("loc",eventList.get(position).getLocation());

                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
