package com.eshael.myevents.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.eshael.myevents.MainActivity;
import com.eshael.myevents.R;
import com.eshael.myevents.VerifyQR;
import com.eshael.myevents.fragments.CategoryEvent;
import com.eshael.myevents.model.Category;
import com.eshael.myevents.model.Ticket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 6/11/2016.
 */
public class CategoryAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<Category> categoryList = new ArrayList<>();
    private Context context;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(layoutInflater == null)
            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = layoutInflater.inflate(R.layout.category_row,null);

        Category category = categoryList.get(position);

        TextView tittle = (TextView) convertView.findViewById(R.id.cat_name);

        tittle.setText(category.getEvent_category());

        convertView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle bundle = new Bundle();
                bundle.putString("category",categoryList.get(position).getEvent_category());
                CategoryEvent fragment = new CategoryEvent();
                fragment.setArguments(bundle);
                ((MainActivity)context).fragmentSwitcher(fragment,categoryList.get(position).getEvent_category()+" Events");
            }
        });

        return convertView;
    }
}
