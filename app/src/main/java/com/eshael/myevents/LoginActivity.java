package com.eshael.myevents;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eshael.myevents.model.Response;
import com.eshael.myevents.util.ConnectionClass;
import com.eshael.myevents.util.Network;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener,Network{

    private EditText username;
    private EditText password;
    private Button loginBtn;
    private ProgressDialog progressDialog;
    private Gson gson;
    private TextView sign_up;
    private String MyPREFERENCES;
    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        setContentView(R.layout.activity_login);

        try
        {
            getSupportActionBar().hide();
        }catch (NullPointerException e){}

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginBtn = (Button)findViewById(R.id.login_btn);
        sign_up = (TextView)findViewById(R.id.sign_up);
        sign_up.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);
        gson = new Gson();


    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.login_btn)
        {
            String userName = username.getText().toString().trim();
            String pwd = password.getText().toString().trim();
            Log.d("Username", userName);
            Log.d("Password", pwd);

            progressDialog.setMessage("Please hold as your account is verified");
            progressDialog.setTitle("MyEvents Login");
            progressDialog.show();

            loginUser(userName,pwd);
        }else if(v.getId() == R.id.sign_up)
        {
            Toast.makeText(LoginActivity.this,"Sign Up",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, SignUp.class);
            startActivity(intent);
            finish();
        }
    }

    private void loginUser(final String username, final String password)
    {
        String[] paramenters = {"username","password"};
        String[] values = {username,password};
        ConnectionClass.ConnectionClass(LoginActivity.this, LOGIN_URL, paramenters, values, " ",
                new ConnectionClass.VolleyCallback()
                {
                    @Override
                    public void onSuccess(String serverstring)
                    {
                        Log.d("Response:", serverstring.toString());

                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(serverstring).getAsJsonObject();

                        Response response = gson.fromJson(obj, Response.class);


                        Log.d("Response:", response.getMessage());
                        if(response.getMessage().equalsIgnoreCase("Success"))
                        {
                            progressDialog.cancel();
                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("username",username);
                            editor.apply();
                            finish();
                            Toast.makeText(LoginActivity.this,"Login Successfull",Toast.LENGTH_LONG).show();
                        }else
                        {
                            progressDialog.cancel();

                            Toast.makeText(LoginActivity.this,"Login failed\nPlease try again",Toast.LENGTH_LONG).show();
                        }

                    }
                 });
    }

}
