package com.eshael.myevents.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ConnectionClass {
    static String finalstring;
    static  Gson gson;

    public static String ConnectionClass(final Context context,String URL_FEED, final String[] parameters
            ,final String[] values, final String tag, final VolleyCallback callback){
        gson = new Gson();

        CustomRequest postRequest = new CustomRequest(Request.Method.POST, URL_FEED, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    VolleyLog.v("Response:", response.toString(4));
                    if(response!=null){
                        Log.d("respo==we have==",response.toString());
                        finalstring =  response.toString();
                            callback.onSuccess(finalstring);

                        AppController.getInstance().cancelPendingRequests(tag);
                    }else{

                        Log.d("null==",response.toString());

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                for(int y =0;y<parameters.length;y++){
                    params.put(parameters[y], values[y]);
                }

                return params;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(postRequest);

        return URL_FEED;
    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }
}
