package com.eshael.myevents.util;

/**
 * Created by Raphael on 6/4/2016.
 */
public interface Network {
    String BASE_URL="http://events-jevents.rhcloud.com/getEvents/";
    String EVENT="http://events-jevents.rhcloud.com/event/";
    String LOGIN_URL="http://events-jevents.rhcloud.com/signin/";
    String BASE_THUMB="http://events-jevents.rhcloud.com";
    String SIGN_UP="http://events-jevents.rhcloud.com/CreateUser";
    String USER_EVENTS="http://events-jevents.rhcloud.com/getTickets/";
    String CATEGORIES = "http://events-jevents.rhcloud.com/getCategories";
    String CATEGORY_EVENT="http://events-jevents.rhcloud.com/category/";
}
