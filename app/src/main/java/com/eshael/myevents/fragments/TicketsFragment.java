package com.eshael.myevents.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.eshael.myevents.R;
import com.eshael.myevents.adapter.TicketAdapter;
import com.eshael.myevents.model.Ticket;
import com.eshael.myevents.util.AppController;
import com.eshael.myevents.util.Network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 6/11/2016.
 */
public class TicketsFragment extends Fragment implements Network{

    private View rootView;
    private TicketAdapter adapter;
    private ListView listView;
    private List<Ticket>ticketList = new ArrayList<>();
    private String MyPREFERENCES;
    private SharedPreferences sharedpreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        sharedpreferences = getContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        rootView = inflater.inflate(R.layout.tickets_fragment,container,false);
        listView = (ListView)rootView.findViewById(R.id.ticket_list);

        adapter = new TicketAdapter(getContext(),ticketList);

        listView.setAdapter(adapter);

        getUserTickets();
        return rootView;
    }

    private void getUserTickets()
    {
        JsonArrayRequest request = new JsonArrayRequest(USER_EVENTS+sharedpreferences.getString("username",""), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {

                Log.d("Ticket",jsonArray+"");
                try
                {
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Ticket ticket = new Ticket();

                        ticket.setEvent_title(jsonObject.getString("event_title"));
                        ticket.setTicket_category(jsonObject.getString("ticket_category"));
                        ticket.setAmount(jsonObject.getString("amount"));
                        ticket.setCode(jsonObject.getString("code"));

                        ticketList.add(ticket);
                    }
                }catch (JSONException e)
                {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });

        AppController.getInstance().addToRequestQueue(request);
    }
}
